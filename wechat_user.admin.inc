<?php
/**
 * page callback.
 */
function wechat_user_bulk_update($form, $form_state) {
  $form['#prefix'] = t('Last update time is @time.', array('@time' => format_date(variable_get('wechat_user_last_update', time()),'medium')));
  $form['update'] = array(
    '#type' => 'submit',
    '#value' => t('Update Wechat users'),
  );
  return $form;
}
/**
 * Form submit.
 */
function wechat_user_bulk_update_submit($form, $form_state) {
  $batch = array(
    'operations' => array(),
    'finished' => '_wechat_bulk_update_users_finish',
    'title' => t('Update Wechat users'),
    'init_message' => t('update is starting...'),
    'progress_message' => t('Processed @current of @total.'),
    'error_message' => t('update has encountered an error.')
  );

  $we_obj = _wechat_init_obj();
  $access_token = wechat_get_access_token();
  $we_obj->checkAuth('', '', $access_token);
  $user_list = $we_obj->getUserList();

  if ($user_list) {
    foreach ($user_list['data']['openid'] as $openid) {
      $batch['operations'][] = array('wechat_user_batch_update_process', array($openid));
    }
  }

  $results = array("Finished", "Thanks for your patient");

  batch_set($batch);
  batch_process('admin/wechat/wechat-users/update'); // The path to redirect to when done.
}
